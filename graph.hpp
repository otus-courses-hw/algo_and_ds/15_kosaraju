#include <vector>
#include <list>
#include <set>

class graph
{
    private:
        using vector = std::vector<long>;
        using adjacency_vector = std::vector<vector>;
        adjacency_vector table;
        std::set<unsigned> visited;

        void dfs_helper(unsigned vertex, std::list<unsigned> &path);

    public:
        graph() = delete;
        ~graph() = default;
        graph(unsigned vertices, unsigned max_degree);
        graph(graph &&other) noexcept;

        void add_edge(std::pair<unsigned, unsigned> vertices);
        [[nodiscard]] graph invert() const;
        std::list<unsigned> dfs(unsigned vertex);
        std::list<unsigned> traverse();
        std::vector<unsigned> connected_components(); /* the target algorithm (kosaraju) */
};
