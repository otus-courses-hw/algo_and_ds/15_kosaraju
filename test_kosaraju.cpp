#include <catch2/catch.hpp>
#include "graph.hpp"

TEST_CASE("happy pass")
{
    unsigned vertices = 8;
    unsigned max_degree = 3;
    graph gr(vertices, max_degree);
    gr.add_edge({0, 1});
    gr.add_edge({1, 2});
    gr.add_edge({1, 4});
    gr.add_edge({1, 5});
    gr.add_edge({2, 3});
    gr.add_edge({2, 6});
    gr.add_edge({3, 2});
    gr.add_edge({3, 7});
    gr.add_edge({4, 0});
    gr.add_edge({4, 5});
    gr.add_edge({5, 6});
    gr.add_edge({6, 5});
    gr.add_edge({7, 3});
    gr.add_edge({7, 6});
    /*         GRAPH
     *      0---x1----x2x---x3
     *      x   /|     |     x
     *      |  / |     |     |
     *      | /  |     |     |
     *      |x   x     x     x
     *      4---x5x---x6x----7
     */
    graph gr_i = gr.invert();
    /*         INVERTED
     *      0x---1x----2x---x3
     *      |   xx     x     x
     *      |  / |     |     |
     *      | /  |     |     |
     *      x/   |     |     x
     *      4x---5x---x6----x7
     */
    SECTION("each dfs call returned a path excluding previous visited vertices.")
    {
        CHECK(gr_i.dfs(0) == std::list<unsigned> {1,4,0});
        CHECK(gr_i.dfs(2) == std::list<unsigned> {7,3,2});
    }
    SECTION("traverse inverted graph. returned path should be inverted.")
    {
        CHECK(gr_i.traverse() == std::list<unsigned> {1,4,0,7,3,2,6,5});
    }
    SECTION("test kosaraju. counting from one in reversed order.")
    {
        CHECK(gr.connected_components() == std::vector<unsigned> {3,3,2,2,3,1,1,2});
        /*                                                        0 1 2 3 4 5 6 7 */
    }
}
