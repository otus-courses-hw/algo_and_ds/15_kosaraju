#include "graph.hpp"
#include <memory>
#include <algorithm>
#include <utility>

graph::graph(unsigned vertices, unsigned max_degree) :
    table(vertices)
{
    for (auto &row : table)
        row.reserve(max_degree);
}

graph::graph(graph &&other) noexcept:
    table(std::move(other.table))
{
}

void graph::add_edge(std::pair<unsigned, unsigned> vertices)
{
    auto &row = table.at(vertices.first);
    
    if (std::find(row.begin(), row.end(), vertices.second) == row.end())
        row.push_back(vertices.second);
}

auto graph::invert() const -> graph
{
    graph inverted(table.size(), table.front().size());

    for (std::size_t vertex_a = 0; vertex_a < table.size(); ++vertex_a)
    {
        auto &row = table.at(vertex_a);

        for (const auto &vertex_b : row)
            inverted.add_edge({vertex_b, vertex_a});
    }

    return inverted;
}

std::list<unsigned> graph::dfs(unsigned vertex)
{
    std::list<unsigned> path;
    dfs_helper(vertex, path);
    return path;
}

void graph::dfs_helper(unsigned vertex, std::list<unsigned> &path)
{
    if (visited.contains(vertex))
        return;

    visited.insert(vertex);

    auto &adjacent = table.at(vertex);

    for (const auto v : adjacent)
    {
        dfs_helper(v, path);

        if (adjacent.back() == v) /* reached the end of path */
            path.push_back(vertex); /* add traversed vertices in reverted order */
    }
}

std::list<unsigned> graph::traverse()
{
    std::list<unsigned> path;

    for (std::size_t vertex = 0; vertex < table.size(); ++vertex)
        dfs_helper(vertex, path);

    return path;
}

std::vector<unsigned> graph::connected_components()
{
    graph gr_i = this->invert();
    std::list<unsigned> path = gr_i.traverse();

    std::vector<unsigned> components(table.size());

    // define a number of connected component each vertex belongs to it
    auto routine = [connection = 1, &components, this] (unsigned vertex) mutable {
        auto path = this->dfs(vertex);

        for (auto v : path)
            components.at(v) = connection;

        if (!path.empty()) // for continuous counting sake
            connection++;
    };

    std::for_each(path.rbegin(), path.rend(), routine); // process vertices in the path in reversed order

    return components;
}